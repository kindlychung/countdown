#!/usr/bin/env python3
##start

import sys
from PySide import QtGui
from PySide import QtCore
from utils.prog import Ui_Prog
import datetime
import subprocess
import os
import platform



class Prog(QtGui.QWidget, Ui_Prog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.timer = QtCore.QTimer()
        self.minutesedit.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.minutesedit.returnPressed.connect(self.checkTime)
        self.stopbtn.clicked.connect(self.timer.stop)

        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        br = QtGui.QDesktopWidget().availableGeometry().bottomRight()
        size = self.size()
        x = br.x() - size.width()/2
        y = br.y() - size.height()/2
        self.move(x, y)


    def countStr(self):
        now = QtCore.QTime.currentTime()
        secsLeft = now.secsTo(self.due)
        if(secsLeft > 0):
            timeStr = str(datetime.timedelta(seconds = secsLeft))
            self.timelabel.setText(timeStr)
        else:
            timeStr = "<font color=red>00:00:00</font>"
            self.timelabel.setText(timeStr)
            self.timer.stop()
            homedir = os.getenv("HOME")
            audiofile = os.path.join(homedir, "audiofiles", "bell.wav")
            osname = platform.system()
            if osname == "Linux":
                subprocess.Popen(["aplay", audiofile])
            else:
                subprocess.Popen(["afplay", audiofile])
        print(timeStr)
        return timeStr

    def checkTime(self):
        if self.timer.isActive():
            self.timer.stop()

        secs = int(self.minutesedit.text()) * 60
        print("You need to wait for %s seconds..." % secs)
        now = QtCore.QTime.currentTime()
        self.due = now.addSecs(secs)

        self.connect(self.timer, QtCore.SIGNAL("timeout()"), self.countStr)
        self.timer.start(1000)




app = QtGui.QApplication(sys.argv)
app.setApplicationName("Countdown")
prog = Prog()
prog.show()
sys.exit(app.exec_())

