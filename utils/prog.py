# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'prog.ui'
#
# Created: Wed Sep 24 20:43:11 2014
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Prog(object):
    def setupUi(self, Prog):
        Prog.setObjectName("Prog")
        Prog.resize(199, 51)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Prog.sizePolicy().hasHeightForWidth())
        Prog.setSizePolicy(sizePolicy)
        Prog.setWindowOpacity(0.8)
        self.horizontalLayout = QtGui.QHBoxLayout(Prog)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.timelabel = QtGui.QLabel(Prog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.timelabel.sizePolicy().hasHeightForWidth())
        self.timelabel.setSizePolicy(sizePolicy)
        self.timelabel.setMinimumSize(QtCore.QSize(77, 21))
        self.timelabel.setScaledContents(False)
        self.timelabel.setAlignment(QtCore.Qt.AlignCenter)
        self.timelabel.setObjectName("timelabel")
        self.horizontalLayout.addWidget(self.timelabel)
        self.minutesedit = QtGui.QLineEdit(Prog)
        self.minutesedit.setMaximumSize(QtCore.QSize(51, 33))
        self.minutesedit.setObjectName("minutesedit")
        self.horizontalLayout.addWidget(self.minutesedit)
        self.stopbtn = QtGui.QPushButton(Prog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.stopbtn.sizePolicy().hasHeightForWidth())
        self.stopbtn.setSizePolicy(sizePolicy)
        self.stopbtn.setMaximumSize(QtCore.QSize(41, 33))
        self.stopbtn.setObjectName("stopbtn")
        self.horizontalLayout.addWidget(self.stopbtn)

        self.retranslateUi(Prog)
        QtCore.QMetaObject.connectSlotsByName(Prog)

    def retranslateUi(self, Prog):
        Prog.setWindowTitle(QtGui.QApplication.translate("Prog", "Countdown", None, QtGui.QApplication.UnicodeUTF8))
        Prog.setAccessibleName(QtGui.QApplication.translate("Prog", "Countdown by minutes", None, QtGui.QApplication.UnicodeUTF8))
        self.timelabel.setText(QtGui.QApplication.translate("Prog", "00:00:00", None, QtGui.QApplication.UnicodeUTF8))
        self.minutesedit.setText(QtGui.QApplication.translate("Prog", "5", None, QtGui.QApplication.UnicodeUTF8))
        self.stopbtn.setText(QtGui.QApplication.translate("Prog", "▣", None, QtGui.QApplication.UnicodeUTF8))

