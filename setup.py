import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os", "sys", "PySide", "datetime", "subprocess"], "excludes": ["tkinter"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"


options = {
    'build_exe': {
        'includes': 'atexit'
    }
}

executables = [
    Executable('countdown.py', base=base)
]

setup(  name = "Countdown",
        version = "0.1",
        description = "Countdown timer",
        options = options,
        executables = executables)
